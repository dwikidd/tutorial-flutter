// ignore_for_file: unnecessary_ , prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';
import './hal_komputer.dart' as komputer;
import './hal_radio.dart' as radio;
import './hal_headset.dart' as headset;
import './hal_smartphone.dart' as hp;

void main() {
  runApp(  MaterialApp(
    title: "Tab Bar",
    home:   Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  late TabController controller;

  @override
  void initState() {
    controller =   TabController(vsync: this, length: 4);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
      return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.purple, Colors.orange])),
       
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(   
          centerTitle: true,     
          title:   Text("Daftar Elektronik"),
          bottom:   TabBar(
            controller: controller,
            tabs: <Widget>[
                Tab(
                icon:   Icon(Icons.computer),
                text: "Komputer",
              ),
                Tab(
                icon:   Icon(Icons.headset),
                text: "Headset",
              ),
                Tab(
                icon:   Icon(Icons.radio),
                text: "Radio",
              ),
                Tab(
                icon:   Icon(Icons.smartphone),
                text: "Smartphone",
              )
            ],
          ),
            backgroundColor: Colors.transparent,
          ),

        body:   TabBarView(
          controller: controller,
          children: <Widget>[
              komputer.Komputer(),
              headset.Headset(),
              radio.Radio(),
              hp.Smartphone(),
          ],
        ),        
        
        bottomNavigationBar:   Material(
          color: Colors.deepPurple,
          child:   TabBar(
            controller: controller,
            tabs: <Widget>[
                Tab(
                icon:   Icon(Icons.computer),
              ),
                Tab(
                icon:   Icon(Icons.headset),
              ),
                Tab(
                icon:   Icon(Icons.radio),
              ),
                Tab(
                icon:   Icon(Icons.smartphone),
              )
            ],
          ),
        ),
      ),
    );
    
  }
}
